import { fighters } from './helpers/mockData';
import { Fighter } from './models/fighter';
import { getFighterDetails } from './services/fightersService';
export async function fight(firstFighter:Fighter, secondFighter:Fighter): Promise<Fighter> {
  let winner:Fighter;
  let healthFighter1 = (await getFighterDetails(firstFighter._id)).health;
  let healthFighter2 = (await getFighterDetails(secondFighter._id)).health;
  while(healthFighter1 > 0 && healthFighter2>0){
    healthFighter2 -= await getDamage(firstFighter, secondFighter);
    healthFighter1 -= await getDamage(secondFighter, firstFighter);
  }
  return healthFighter2 > healthFighter1 ? secondFighter: firstFighter;
}

export async function getDamage(attacker:Fighter, enemy:Fighter) {
  
  const damage = await getHitPower(attacker) - await getBlockPower(enemy);
  
  return damage;
}

export async function getHitPower(fighter:Fighter) {
  const details = await getFighterDetails(fighter._id);
  const criticalHitChance:number = Math.random()*2+1;
  return details.attack*criticalHitChance;
}

export async function getBlockPower(fighter:Fighter) {
  const details = await getFighterDetails(fighter._id);
  const dodgeChance:number = Math.random()*2+1;
  return details.defense*dodgeChance;
}
