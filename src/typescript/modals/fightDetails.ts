import { Details } from './../models/details';
import { fighters } from './../helpers/mockData';
import { Fighter } from './../models/fighter';
import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';
import { getFighterDetails } from '../services/fightersService';

export async function showFightDetailsModal(firstFighter: Fighter, secondFighter: Fighter) {
  const title = 'Fight   in the proccess.';
  const bodyElement = await createFightDetails(firstFighter, secondFighter);
  showModal({ title, bodyElement });
}

async function createFightDetails(firstFighter: Fighter, secondFighter: Fighter): Promise<HTMLElement> {
  const { name: name1, attack: attack1, defense: defense1, health: health1, source: image1 } = await getFighterDetails(firstFighter._id);
  const { name: name2, attack: attack2, defense: defense2, health: health2, source: image2 } = await getFighterDetails(secondFighter._id);

  const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });
  const nameElement = createElement({ tagName: 'span', className: 'fighter-name' });
  const attackStatElement = createElement({ tagName: 'div', className: 'fighter-attack' });
  const defenseStatElement = createElement({ tagName: 'div', className: 'fighter-defense' });
  const healthStatElement = createElement({ tagName: 'div', className: 'fighter-health' });

  const row = createElement({ tagName: 'div', className: 'image-row' });
  const imageStatElement = createElement({ tagName: 'img', className: 'firstFighter-image', attributes: { src: image1 } });
  const imageStatElement1 = createElement({ tagName: 'img', className: 'secondFighter-image', attributes: { src: image2 } });

  nameElement.innerText = `Name: ${name1} vs ${name2}`;
  attackStatElement.innerText = `Attack: ${attack1} vs ${attack2}`;
  defenseStatElement.innerText = `Defense: ${defense1} vs ${defense2}`;
  healthStatElement.innerText = `Health: ${health1} vs ${health2}`;

  row.append(imageStatElement, imageStatElement1);
  fighterDetails.append(nameElement, attackStatElement, defenseStatElement, healthStatElement, row);
  return fighterDetails;
}
