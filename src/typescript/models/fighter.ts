export interface Fighter {
    _id: number,
    name: string,
    source: string
}